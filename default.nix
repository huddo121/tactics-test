let
  config = (import ./config.nix);
  hpkgs = config.nixpkgs.haskellPackages;
in hpkgs.${config.projectName}
