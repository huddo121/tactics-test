module BattleGrid where

import SDL (V4(..))
import GHC.Word (Word8(..))

data BattleGrid = BattleGrid [[BattleTile]] deriving (Eq, Show)
data BattleTile = BattleTile Material TileHeight deriving (Eq, Show)
data Material = Void | Grass | Rock deriving (Eq, Show)

type TileColours = (V4 Word8, V4 Word8, V4 Word8)
coloursFor :: Material -> TileColours
coloursFor Void = (V4 0 0 0 0, V4 0 0 0 0, V4 0 0 0 0)
coloursFor Grass = (V4 0 255 0 255, V4 0 210 0 255, V4 0 180 0 255)
coloursFor Rock = (V4 40 50 40 255, V4 30 35 30 255, V4 18 24 18 255)

battleTiles (BattleGrid t) = t
battleTileHeight (BattleTile _ (TileHeight h)) = h
battleTileMaterial (BattleTile m _) = m

voidTile = BattleTile Void (TileHeight 0) -- Void tiles don't get rendered, no height necessary
grassTile = BattleTile Grass . TileHeight
rockTile = BattleTile Rock . TileHeight

testGrid = BattleGrid
  [ [grassTile 4, grassTile 4, grassTile 4, grassTile 4]
  , [voidTile, rockTile 6, rockTile 6, rockTile 2]
  , [voidTile, rockTile 6, rockTile 6, rockTile 2]
  , [voidTile, rockTile 2, rockTile 2, rockTile 2]
  ]

tileWidth = 100
-- | When due to the isometric perspective, each row needs to be renderered
--   some distance to the right of the preceding row.
tileXShift = tileWidth `div` 2
tileHeight = 25
maxHeight = 20
minHeight = 0

newtype TileHeight = TileHeight Integer deriving (Eq, Ord, Show)

instance Bounded TileHeight where
  minBound = TileHeight 0
  maxBound = TileHeight 20


oneLine = BattleGrid
  [ [ grassTile 1
    , grassTile 1
    , grassTile 1
    , grassTile 1
    , grassTile 1
    , grassTile 1
    ]
  ]

sparta = BattleGrid [[voidTile  , voidTile  ,grassTile 3,grassTile 3, voidTile  , voidTile  ]
                    ,[voidTile  , voidTile  ,grassTile 2,grassTile 2, voidTile  , voidTile  ]
                    ,[voidTile  , voidTile  ,grassTile 1,grassTile 1, voidTile  , voidTile  ]
                    ,[rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1]
                    ,[rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1]
                    ,[rockTile 1, rockTile 1, voidTile  , voidTile  , rockTile 1, rockTile 1]
                    ,[rockTile 1, rockTile 1, voidTile  , voidTile  , rockTile 1, rockTile 1]
                    ,[rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1]
                    ,[rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1, rockTile 1]]


gradientGrid =
  BattleGrid [[grassTile 4, grassTile 3, grassTile 2, grassTile 2, grassTile 2, grassTile 1]]
