module Main where

import           Control.Monad.State
import           SDL
import           SDL.Font
import           SDL.Primitive
import           Control.Monad                  ( unless, forM_ )
import           Control.Monad.IO.Class         ( MonadIO(..) )
import           Data.Vector.Storable           ( fromList )
import qualified Data.Text as T

import BattleGrid

main :: IO ()
main = do
  initializeAll
  SDL.Font.initialize
  window   <- createWindow "Tactics-Test" defaultWindow
  renderer <- createRenderer window (-1) defaultRenderer
  appLoop renderer

appLoop :: (MonadIO m) => Renderer -> m ()
appLoop renderer = do
  events <- pollEvents
  let eventIsQPress event = case eventPayload event of
        KeyboardEvent keyboardEvent ->
          keyboardEventKeyMotion keyboardEvent
            == Pressed
            && keysymKeycode (keyboardEventKeysym keyboardEvent)
            == KeycodeQ
        _ -> False
      qPressed = any eventIsQPress events
  rendererDrawColor renderer $= V4 0 0 0 0
  clear renderer

  -- renderBattleTiles renderer (battleTiles testGrid) 100 200
  renderBattleTiles renderer (battleTiles sparta) 50 50
  overlay <- createRGBSurface (V2 200 100) ARGB4444
  writeText "Henlo" overlay
  -- renderBattleTiles renderer (battleTiles gradientGrid) 100 200
  -- renderBattleTiles renderer (battleTiles oneLine) 100 200

  present renderer
  unless qPressed (appLoop renderer)

renderBattleTiles renderer tiles offsetX offsetY = forM_ indexedTiles renderWithIndexOffset
                          where indexedTiles = zip [0..] tiles
                                renderWithIndexOffset (idx, row) = renderBattleTileRow renderer row (fromIntegral $ offsetX + (idx * tileXShift)) (fromIntegral $ offsetY + (idx * tileHeight))

renderBattleTileRow :: MonadIO m => Renderer -> [BattleTile] -> Integer -> Integer -> m ()
renderBattleTileRow renderer tiles offsetX offsetY = forM_ (reverse indexedTiles) renderWithIndexOffset
                          where indexedTiles = zip [0..] tiles
                                renderWithIndexOffset (idx, tile) = renderTile renderer tile (fromIntegral $ idx * tileXShift + offsetX) (individualTileHeightOffset tile - idx * tileHeight)
                                individualTileHeightOffset tile = fromIntegral $ offsetY + tileHeight * (5 - (battleTileHeight tile)) -- TODO: Replace that 5 with maxHeight, compensate for it

renderTile renderer tile offsetX offsetY = do
  renderTileBase renderer tile offsetX (fromIntegral $ offsetY + tileWidth `div` 4)
  renderTileTop renderer
                tile
                (fromIntegral offsetX)
                (fromIntegral offsetY)

renderTileTop renderer tile offsetX offsetY = fillPolygon renderer
                                                           xs
                                                           ys
                                                           colour
 where
  half    = tileWidth `div` 2
  quarter = half `div` 2
  xs      = fromList $ fromIntegral . (+ offsetX) <$> [0, half, tileWidth, half]
  ys      = fromList $ fromIntegral . (+ offsetY) <$> [quarter, 0, quarter, half]
  (colour, _, _)  = coloursFor $ battleTileMaterial tile

renderTileBase renderer tile offsetX offsetY = do
  fillRectangle renderer (V2 x1 y1) (V2 x3 y3) darkSide
  fillRectangle renderer (V2 x1 y1) (V2 x2 y2) lightSide
 where
  x1 = fromIntegral $ offsetX
  y1 = fromIntegral $ offsetY
  x2 = fromIntegral $ offsetX + (tileWidth `div` 2)
  y2 = fromIntegral $ offsetY + 300
  x3 = fromIntegral $ offsetX + tileWidth
  y3 = fromIntegral $ y2
  (_, lightSide, darkSide) = coloursFor (battleTileMaterial tile)

red :: SDL.Font.Color
red = SDL.V4 255 0 0 0

writeText :: MonadIO m => T.Text -> Surface -> m ()
writeText text surface = do
  font <- load "./AlegreyaSansSC-Thin.ttf" 100
  text <- solid font red text
  -- SDL.Font.free font
  SDL.surfaceBlit text Nothing surface Nothing
  -- SDL.freeSurface text
  pure ()
